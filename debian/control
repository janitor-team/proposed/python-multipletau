Source: python-multipletau
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Alexandre Mestiashvili <mestia@debian.org>
Section: python
Priority: optional
Build-Depends: debhelper-compat (= 12),
               dh-python,
               python3-all,
               python3-mock,
               python3-numpy,
               python3-numpydoc,
               python3-pytest,
               python3-pytest-runner,
               python3-setuptools,
               python3-sphinx
Standards-Version: 4.4.1
Vcs-Browser: https://salsa.debian.org/med-team/python-multipletau
Vcs-Git: https://salsa.debian.org/med-team/python-multipletau.git
Homepage: https://github.com/FCS-analysis/multipletau

Package: python3-multipletau
Architecture: all
Depends: ${misc:Depends},
         ${python3:Depends}
Description: multiple-tau algorithm for Python3/NumPy
 Multiple-tau correlation is computed on a logarithmic scale (less
 data points are computed) and is thus much faster than conventional
 correlation on a linear scale such as `numpy.correlate`
 .
 An online reference is available
 at http://paulmueller.github.io/multipletau
 .
 This is the Python 3 version of the package

Package: python-multipletau-doc
Architecture: all
Section: doc
Depends: libjs-mathjax,
         ${misc:Depends},
         ${sphinxdoc:Depends}
Description: documentation for multipletau Python module
 This package contains HTML documentation for python-multipletau
 .
 An online reference is available
 at http://paulmueller.github.io/multipletau
